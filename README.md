Overview 
======================================================================

Knock Knock jokes are fun. Let write something in javascript to do
them.

Design Direction
======================================================================

This should feel like an interaction with the computer. Like it's
playing the game with you. This means it should be visually pleasing. You should see you knocks echo on the screen.

```
\ | /  \ | /
- O -  - O -
/ | \  / | \
```

Those are knocks. The "O" are where you tapped or clicked. The visual
radidation of sound are the other marks. The sound should radiate away
from where you tapped or clicked. This allows us to fill two sense to
convey the same information. The user will hear the click or tap and
we will display the click or tap. What they hear will be something
from the real world and what they see will be something from the
computer world.

It would be awesome if we could use their Mic to capture the sound of
the tap and have the sound radidation go off of that.


Requirements
======================================================================

- SHOULD have jokes
- SHOULD handle errors in a funny way
- SHOULD only allow two knocks


